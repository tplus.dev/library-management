package tools.tplus.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.logging.Logger;

public class HibernateUtil {

    private static final Logger LOGGER = Logger.getLogger(HibernateUtil.class.getName());

    private static SessionFactory sessionFactory;

    private HibernateUtil() {}


    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            return new Configuration().configure().buildSessionFactory();
        } catch (Exception e) {
            // Make sure you log the exception, as it might be swallowed
            LOGGER.warning("Can not read hibernate.cfg.xml");
            throw new RuntimeException(e);
        }
    }

    public static SessionFactory getSession() {
        if (sessionFactory == null) {
            sessionFactory = buildSessionFactory();
        }
        return sessionFactory;
    }

}