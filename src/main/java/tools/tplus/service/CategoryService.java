package tools.tplus.service;

import org.hibernate.Session;
import tools.tplus.model.Category;
import tools.tplus.util.HibernateUtil;

import java.util.List;

public class CategoryService {

    public static List<Category> getAll() {
        String sql = "FROM Category c ORDER BY c.id ASC";
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();

        @SuppressWarnings("unchecked")
        List<Category> categories = (List<Category>) session.createQuery(sql).list();

        session.getTransaction().commit();
        session.close();

        return categories;
    }

}
