package tools.tplus.service;

import org.hibernate.Session;
import tools.tplus.model.BookList;
import tools.tplus.model.BookLoan;
import tools.tplus.model.Student;
import tools.tplus.util.HibernateUtil;

import java.time.LocalDate;
import java.util.List;

public class StudentService {

    private static List<Student> find(String sql) {
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();

        @SuppressWarnings("unchecked")
        List<Student> students = (List<Student>) session.createQuery(sql).list();

        session.getTransaction().commit();
        session.close();

        return students;
    }

    public static Student getByCode(String code) {
        final String sql = "FROM Student s WHERE s.code = '" + code + "'";
        List<Student> students = find(sql);
        if (students.isEmpty()) {
            return null;
        }
        return students.get(0);
    }

    public static List<Student> getStudentList() {
        final String sql = "FROM Student s ORDER BY s.code ASC";
        return find(sql);
    }

    public static void saveOrUpdate(Student student) {
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();
        session.saveOrUpdate(student);
        session.getTransaction().commit();
        session.close();
    }

    public static String nextCode() {
        final String sql = "FROM Student s ORDER BY s.code DESC";
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();
        @SuppressWarnings("unchecked")
        List<Student> students = (List<Student>) session.createQuery(sql).setMaxResults(1).list();
        session.getTransaction().commit();
        session.close();

        Student student = students.get(0);
        int next = Integer.parseInt(student.getCode().substring(1));
        ++next;
        return String.format("S%04d", next);
    }

    public static boolean delete(String code) {
        Student student = getByCode(code);
        if (student == null) {
            return false;
        }

        for(BookLoan b: student.getBookLoans()) {
            if (b.getReturnDate() == null) {
                return false;
            }
        }

        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();
        student.getBookLoans().forEach(session::delete);
        session.delete(student);
        session.getTransaction().commit();
        session.close();

        return true;
    }

    public static void loanReturn(String studentCode, String bookCode) {
        Student student = getByCode(studentCode);
        assert student != null;
        student.getBookLoans().forEach(bookLoan -> {
            if (bookLoan.getBook().getCode().equals(bookCode) && bookLoan.getReturnDate() == null) {
                bookLoan.setReturnDate(LocalDate.now());


                Session session = HibernateUtil.getSession().openSession();
                session.beginTransaction();

                //Update BookLoan
                session.saveOrUpdate(bookLoan);

                //Update BookList
                BookList bookList = BookListService.getByCode(bookCode);
                assert bookList != null;
                int amount = bookList.getAmount();
                ++amount;
                bookList.setAmount(amount);
                session.saveOrUpdate(bookList);

                session.getTransaction().commit();
                session.close();
            }
        });
    }
}
