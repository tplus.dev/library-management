package tools.tplus.service;

import org.hibernate.Session;
import tools.tplus.model.BookLoan;
import tools.tplus.util.HibernateUtil;

import java.util.List;

public class BookLoanService {

    public static void removeByBookCode(String bookCode) {
        String sql = "FROM BookLoan bl WHERE bl.book.code = '" + bookCode + "'";
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();
        find(sql).forEach(session::delete);
        session.getTransaction().commit();
        session.close();
    }

    public static boolean isLoaning(String bookCode) {
        String sql = "FROM BookLoan bl WHERE bl.book.code = '" + bookCode + "' AND bl.returnDate IS NULL";
        return !find(sql).isEmpty();
    }

    private static List<BookLoan> find(String sql) {
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();

        @SuppressWarnings("unchecked")
        List<BookLoan> bookLoans = (List<BookLoan>) session.createQuery(sql).list();

        session.getTransaction().commit();
        session.close();

        return bookLoans;
    }

    public static void saveOrUpdate(BookLoan bookLoan) {
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();
        session.saveOrUpdate(bookLoan);
        session.getTransaction().commit();
        session.close();
    }
}
