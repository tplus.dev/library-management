package tools.tplus.service;

import org.hibernate.Session;
import tools.tplus.model.BookList;
import tools.tplus.util.HibernateUtil;

import java.util.List;

public class BookListService {

    public static List<BookList> getBookLists() {
        final String sql = "FROM BookList bl ORDER BY bl.book.code ASC";
        return find(sql);
    }

    public static BookList getByCode(String code) {
        final String sql = "FROM BookList bl WHERE bl.book.code = '" + code + "'";
        List<BookList> bookLists = find(sql);
        if (bookLists.isEmpty()) {
            return null;
        }
        return bookLists.get(0);
    }

    public static String nextCode() {
        final String sql = "FROM BookList bl ORDER BY bl.book.code DESC";
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();
        @SuppressWarnings("unchecked")
        List<BookList> bookLists = (List<BookList>) session.createQuery(sql).setMaxResults(1).list();
        session.getTransaction().commit();
        session.close();

        BookList bookList = bookLists.get(0);
        int next = Integer.parseInt(bookList.getBook().getCode().substring(1));
        ++next;
        return String.format("B%04d", next);
    }

    public static void saveOrUpdate(BookList bookList) {
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();
        session.saveOrUpdate(bookList);
        session.getTransaction().commit();
        session.close();
    }

    private static List<BookList> find(String sql) {
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();

        @SuppressWarnings("unchecked")
        List<BookList> bookLists = (List<BookList>) session.createQuery(sql).list();

        session.getTransaction().commit();
        session.close();

        return bookLists;
    }

    private static void removeByBookCode(String bookCode) {
        String sql = "FROM BookList bl WHERE bl.book.code = '" + bookCode + "'";
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();
        find(sql).forEach(session::delete);
        session.getTransaction().commit();
        session.close();
    }

    public static boolean delete(String code) {
        if (BookLoanService.isLoaning(code)) {
            return false;
        }

        BookLoanService.removeByBookCode(code);
        removeByBookCode(code);
        return true;
    }
}
