package tools.tplus.service;

import org.hibernate.Session;
import tools.tplus.model.Librarian;
import tools.tplus.util.HibernateUtil;

import java.util.List;

public class LibrarianService {

    public static Librarian login(String user, String pass) {
        final String sql = "FROM Librarian l WHERE l.username = '" + user + "' AND l" +
                ".password = '" + pass + "'";
        Session session = HibernateUtil.getSession().openSession();
        session.beginTransaction();

        @SuppressWarnings("unchecked")
        List<Librarian> librarians = (List<Librarian>) session.createQuery(sql).list();

        session.getTransaction().commit();
        session.close();

        if (librarians.isEmpty()) return null;

        return librarians.get(0);
    }
}
