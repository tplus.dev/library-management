package tools.tplus.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    @Id
    @GeneratedValue
    private Long id;
    private String code;
    private String title;
    private String author;

    @Column(columnDefinition="TEXT")
    private String description;

    @ManyToOne
    private Category category;

    public static Book createEmpty() {
        return new Book(null, "","","","", Category.createEmpty());
    }
}
