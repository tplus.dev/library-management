package tools.tplus.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String code;
    private String major;

    @OneToMany(fetch = FetchType.EAGER)
    private List<BookLoan> bookLoans;

    public static Student createEmpty() {
        return new Student(null, "","","","","", null);
    }
}
