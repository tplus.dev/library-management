package tools.tplus.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Category {

    private static final String DEFAULT_VALUE = "Chính trị – Pháp luật";

    @Id
    @GeneratedValue
    private Long id;
    private String name;

    public static Category createEmpty() {
        return new Category(null, DEFAULT_VALUE);
    }
}
