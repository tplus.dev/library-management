/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools.tplus.frame;

import tools.tplus.model.BookList;
import tools.tplus.model.BookLoan;
import tools.tplus.model.Librarian;
import tools.tplus.model.Student;
import tools.tplus.service.BookListService;
import tools.tplus.service.StudentService;

import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.text.Normalizer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Vector;
import java.util.regex.Pattern;

/**
 *
 * @author TPlus
 */
public class MainFrame extends javax.swing.JFrame {

    private final Librarian librarian;
    private Vector<Vector<String>> bookData;
    private Vector<Vector<String>> studentData;

    public MainFrame(Librarian librarian) {
        this.librarian = librarian;
        initComponents();
        init();
    }

    private void init() {
        //Show center of screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        //Show welcome librarian
        txtWelcomeName.setText(librarian.getName());

        initBooks();
        initStudent();
        initBookLoan();
    }

    private void initBookLoan() {
        Vector<Vector<String>> data = new Vector<>();
        studentData.forEach(d -> {
            Vector<String> row = new Vector<>();
            row.add(d.get(0));
            row.add(d.get(1));
            data.add(row);
        });

        setStudentLoanInfo(data);
    }

    private void setStudentLoanInfo(Vector<Vector<String>> data) {
        Vector<String> cols = new Vector<>();
        cols.add("Mã đọc giả");
        cols.add("Tên đọc giả");

        Vector<Vector<String>> temp = new Vector<>();
        data.forEach(d -> {
            Vector<String> row = new Vector<>();
            row.add(d.get(0));
            row.add(d.get(1));
            temp.add(row);
        });

        tbLoanStudent.setModel(new DefaultTableModel(temp, cols));
        if (!temp.isEmpty()) {
            tbLoanStudent.setRowSelectionInterval(0, 0);
            getBookLoan(0);
        }
    }

    private void getBookLoan(int selectedRow) {
        DefaultTableModel model = (DefaultTableModel) tbLoanStudent.getModel();
        String studentCode = (String) model.getValueAt(selectedRow, 0);

        Vector<Vector<String>> data = new Vector<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        Student student = StudentService.getByCode(studentCode);
        assert student != null;
        for(BookLoan b: student.getBookLoans()) {
            Vector<String> row = new Vector<>();
            row.add(b.getBook().getCode());
            row.add(b.getBook().getTitle());
            row.add(b.getLoanDate().format(formatter));
            row.add(b.getLoanDate().plusDays(b.getLoanDays()).format(formatter));
            String returnDate = b.getReturnDate() == null ? "" : b.getReturnDate().format(formatter);
            row.add(returnDate);
            row.add(b.getLibrarian().getName());
            String status = returnDate.isBlank() ? "Chưa trả" : "Đã trả";
            row.add(status);
            data.add(row);
        }
        setBookLoanInfo(data);
    }

    private void setBookLoanInfo(Vector<Vector<String>> data) {
        Vector<String> cols = new Vector<>();
        cols.add("Mã sách");
        cols.add("Tên sách");
        cols.add("Ngày mượn");
        cols.add("Hạn trả");
        cols.add("Ngày trả");
        cols.add("Người duyệt");
        cols.add("Trạng thái");

        tbLoanBook.setModel(new DefaultTableModel(data, cols));
        if (!data.isEmpty()) {
            tbLoanBook.setRowSelectionInterval(0, 0);
        }
    }

    private void loanReturn() {
        int selectedRow = tbLoanBook.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) tbLoanBook.getModel();

        String returnDate = (String) model.getValueAt(selectedRow, 4);
        if (!returnDate.isBlank()) {
            InfoDialog infoDialog = new InfoDialog(this, true, "Sách này đã được trả!");
            infoDialog.setLocationRelativeTo(this);
            infoDialog.setVisible(true);
            return;
        }

        LoanReturnDialog dialog = new LoanReturnDialog(this, true);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);

        if(dialog.getCode() == LoanReturnDialog.YES) {
            int i = tbLoanStudent.getSelectedRow();
            DefaultTableModel m = (DefaultTableModel) tbLoanStudent.getModel();

            String bookCode = (String) model.getValueAt(selectedRow, 0);
            String studentCode = (String) m.getValueAt(i, 0);

            StudentService.loanReturn(studentCode, bookCode);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            model.setValueAt(LocalDate.now().format(formatter), selectedRow, 4);
            model.setValueAt("Đã trả", selectedRow, 6);
            model.fireTableDataChanged();
            tbLoanBook.setRowSelectionInterval(selectedRow, selectedRow);
            initBooks();
        }
    }

    private void loanBorrow() {
        int studentRow = tbLoanStudent.getSelectedRow();
        DefaultTableModel studentModel = (DefaultTableModel) tbLoanStudent.getModel();
        String studentCode = studentModel.getValueAt(studentRow, 0).toString();

        LoanBorrowDialog dialog = new LoanBorrowDialog(this, true, librarian, studentCode);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        if (dialog.getCode() == LoanBorrowDialog.OK) {
            BookLoan bookLoan = dialog.getBookLoan();
            Vector<String> row = new Vector<>();
            row.add(bookLoan.getBook().getCode());
            row.add(bookLoan.getBook().getTitle());
            row.add(bookLoan.getLoanDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            row.add(bookLoan.getLoanDate().plusDays(bookLoan.getLoanDays()).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            row.add(bookLoan.getReturnDate() == null ? "" :
                    bookLoan.getReturnDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            row.add(bookLoan.getLibrarian().getName());

            DefaultTableModel bookModel = (DefaultTableModel) tbLoanBook.getModel();
            bookModel.addRow(row);
            bookModel.fireTableDataChanged();
            int i = bookModel.getRowCount() - 1;
            tbLoanBook.setRowSelectionInterval(i,i);
            initBooks();
        }
    }

    private boolean compare(String keyword, String contents) {
        keyword = deAccent(keyword.toLowerCase());
        contents = deAccent(contents.toLowerCase());
        return contents.contains(keyword);
    }

    private String deAccent(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }



    private void initStudent() {
        java.util.List<Student> students = StudentService.getStudentList();

        studentData = new Vector<>();
        students.forEach(s -> {
            Vector<String> row = new Vector<>();
            row.add(s.getCode());
            row.add(s.getName());
            row.add(s.getPhone());
            row.add(s.getEmail());
            row.add(s.getMajor());
            studentData.add(row);
        });

        setStudentData(studentData);
        initBookLoan();
    }

    private void setStudentData(Vector<Vector<String>> studentData) {
        Vector<String> cols = new Vector<>();
        cols.add("Mã đọc giả");
        cols.add("Tên đọc giả");
        cols.add("Số điện thoại");
        cols.add("Email");
        cols.add("Chuyên ngành");

        tbStudent.setModel(new DefaultTableModel(studentData, cols));
        if (!studentData.isEmpty()) {
            tbStudent.setRowSelectionInterval(0, 0);
            setStudentInfo(0);
        }
    }

    private void setStudentInfo(int selectedRow) {
        DefaultTableModel model = (DefaultTableModel) tbStudent.getModel();

        txtStudentCode.setText(model.getValueAt(selectedRow, 0).toString());
        txtStudentName.setText(model.getValueAt(selectedRow, 1).toString());
        txtStudentPhone.setText(model.getValueAt(selectedRow, 2).toString());
        txtStudentEmail.setText(model.getValueAt(selectedRow, 3).toString());
        txtStudentMajor.setText(model.getValueAt(selectedRow, 4).toString());
    }

    private void searchStudent(boolean isLoan) {
        String keyword;
        if (isLoan) {
            keyword = txtLoanSearch.getText();
        } else {
            keyword = txtStudentSearch.getText();
        }

        if (keyword.isBlank()) {
            if (isLoan) {
                initBookLoan();
            } else {
                initStudent();
            }
            return;
        }

        Vector<Vector<String>> searchData = new Vector<>();
        int indexSearchBy;
        if (isLoan) {
            indexSearchBy = cbLoanSearch.getSelectedIndex();
        } else {
            indexSearchBy = cbStudentSearchBy.getSelectedIndex();
        }

        studentData.forEach(row -> {
            boolean found = compare(keyword, row.get(indexSearchBy));
            if (found) {
                searchData.add(row);
            }
        });

        if(isLoan) {
            setStudentLoanInfo(searchData);
        } else {
            setStudentData(searchData);
        }

    }

    private void viewStudent() {
        DefaultTableModel model = (DefaultTableModel) tbStudent.getModel();
        int selectedRow = tbStudent.getSelectedRow();
        String studentCode = (String) model.getValueAt(selectedRow, 0);

        StudentViewDialog dialog = new StudentViewDialog(this, true, studentCode);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    }

    private void deleteStudent() {
        DeleteDialog deleteDialog = new DeleteDialog(this, true);
        deleteDialog.setLocationRelativeTo(this);
        deleteDialog.setVisible(true);
        if (deleteDialog.getCode() == DeleteDialog.YES) {
            DefaultTableModel model = (DefaultTableModel) tbStudent.getModel();
            int selectedRow = tbStudent.getSelectedRow();
            String code = (String) model.getValueAt(selectedRow, 0);

            boolean delete = StudentService.delete(code);
            String msg = delete ? "Đã xóa đọc giả!" : "Không thể xóa đọc giả vì có một số sách chưa trả!";
            InfoDialog infoDialog = new InfoDialog(this, true, msg);
            infoDialog.setLocationRelativeTo(this);
            infoDialog.setVisible(true);
            if (delete) {
                initStudent();
            }
        }
    }

    private void editStudent() {
        DefaultTableModel model = (DefaultTableModel) tbStudent.getModel();
        int selectedRow = tbStudent.getSelectedRow();
        String code = (String) model.getValueAt(selectedRow, 0);

        Student student = StudentService.getByCode(code);
        addEditStudent(student);
    }

    private void addStudent() {
        addEditStudent(new Student());
    }

    private void addEditStudent(Student student) {
        StudentAddEditDialog dialog = new StudentAddEditDialog(this, true, student);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        if (dialog.getCode() == StudentAddEditDialog.OK) {
            initStudent();
        }
    }



    private void initBooks() {
        java.util.List<BookList> bookListList = BookListService.getBookLists();

        bookData = new Vector<>();
        bookListList.forEach(bl -> {
            Vector<String> row = new Vector<>();
            row.add(bl.getBook().getCode());
            row.add(bl.getBook().getTitle());
            row.add(bl.getBook().getAuthor());
            row.add(bl.getBook().getCategory().getName());
            row.add(bl.getBook().getDescription());
            row.add(bl.getAmount().toString());
            bookData.add(row);
        });

        setBookData(bookData);
    }

    private void setBookData(Vector<Vector<String>> data) {
        Vector<String> cols = new Vector<>();
        cols.add("Mã sách");
        cols.add("Tên sách");
        cols.add("Tác giả");
        cols.add("Thể loại");
        cols.add("Mô tả");
        cols.add("Số lượng còn lại");

        tbBook.setModel(new DefaultTableModel(data, cols));
        if (!data.isEmpty()) {
            tbBook.setRowSelectionInterval(0, 0);
            setBookInfo(0);
        }
    }

    private void setBookInfo(int selectedRow) {
        DefaultTableModel model = (DefaultTableModel) tbBook.getModel();

        txtBookCode.setText(model.getValueAt(selectedRow, 0).toString());
        txtBookName.setText(model.getValueAt(selectedRow, 1).toString());
        txtBookAuth.setText(model.getValueAt(selectedRow, 2).toString());
        txtBookCategory.setText(model.getValueAt(selectedRow, 3).toString());
        txtBookDescription.setText("<html>" + model.getValueAt(selectedRow, 4).toString() + "</html>");
    }

    private void searchBook() {
        String keyword = txtBookSearch.getText();
        if (keyword.isBlank()) {
            initBooks();
            return;
        }

        Vector<Vector<String>> searchData = new Vector<>();
        int indexSearchBy = cbBookSearchBy.getSelectedIndex();
        bookData.forEach(row -> {
            boolean found = compare(keyword, row.get(indexSearchBy));
            if (found) {
                searchData.add(row);
            }
        });
        setBookData(searchData);
    }

    private void viewBook() {
        DefaultTableModel model = (DefaultTableModel) tbBook.getModel();
        int selectedRow = tbBook.getSelectedRow();
        String bookListCode = (String) model.getValueAt(selectedRow, 0);

        BookViewDialog bookViewDialog = new BookViewDialog(this, true, bookListCode);
        bookViewDialog.setLocationRelativeTo(this);
        bookViewDialog.setVisible(true);
    }

    private void deleteBook() {
        DeleteDialog deleteDialog = new DeleteDialog(this, true);
        deleteDialog.setLocationRelativeTo(this);
        deleteDialog.setVisible(true);
        if (deleteDialog.getCode() == DeleteDialog.YES) {
            DefaultTableModel model = (DefaultTableModel) tbBook.getModel();
            int selectedRow = tbBook.getSelectedRow();
            String code = (String) model.getValueAt(selectedRow, 0);

            boolean delete = BookListService.delete(code);
            String msg = delete ? "Đã xóa sách ra khỏi thư viện!" : "Không thể xóa sách vì có một số đọc giả chưa trả sách!";
            InfoDialog infoDialog = new InfoDialog(this, true, msg);
            infoDialog.setLocationRelativeTo(this);
            infoDialog.setVisible(true);
            if (delete) {
                initBooks();
            }
        }
    }

    private void editBook() {
        DefaultTableModel model = (DefaultTableModel) tbBook.getModel();
        int selectedRow = tbBook.getSelectedRow();
        String bookListCode = (String) model.getValueAt(selectedRow, 0);

        BookList bookList = BookListService.getByCode(bookListCode);
        addEditBook(bookList);
    }

    private void addBook() {
        addEditBook(new BookList());
    }

    private void addEditBook(BookList bookList) {
        BookAddEditDialog addEditDialog = new BookAddEditDialog(this, true, bookList);
        addEditDialog.setLocationRelativeTo(this);
        addEditDialog.setVisible(true);
        if (addEditDialog.getCode() == BookAddEditDialog.OK) {
            initBooks();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtBookSearch = new javax.swing.JTextField();
        cbBookSearchBy = new javax.swing.JComboBox<>();
        btnBookSearch = new javax.swing.JButton();
        btnBookClear = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbBook = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        txtBookCode = new javax.swing.JLabel();
        txtBookName = new javax.swing.JLabel();
        txtBookAuth = new javax.swing.JLabel();
        txtBookCategory = new javax.swing.JLabel();
        txtBookDescription = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        txtBookAdd = new javax.swing.JLabel();
        txtBookEdit = new javax.swing.JLabel();
        txtBookDelete = new javax.swing.JLabel();
        txtBookView = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtStudentSearch = new javax.swing.JTextField();
        cbStudentSearchBy = new javax.swing.JComboBox<>();
        btnStudentSearch = new javax.swing.JButton();
        btnStudentClear = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbStudent = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        txtStudentCode = new javax.swing.JLabel();
        txtStudentName = new javax.swing.JLabel();
        txtStudentPhone = new javax.swing.JLabel();
        txtStudentEmail = new javax.swing.JLabel();
        txtStudentMajor = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        txtStudentAdd = new javax.swing.JLabel();
        txtStudentEdit = new javax.swing.JLabel();
        txtStudentDelete = new javax.swing.JLabel();
        txtStudentView = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        txtLoanSearch = new javax.swing.JTextField();
        cbLoanSearch = new javax.swing.JComboBox<>();
        btnLoanClear = new javax.swing.JButton();
        btnLoanSearch = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbLoanStudent = new javax.swing.JTable();
        jLabel17 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbLoanBook = new javax.swing.JTable();
        btnLoanBorrow = new javax.swing.JButton();
        btnLoanReturn = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtWelcomeName = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Quản lý Thư viện");
        setResizable(false);

        jLabel1.setText("Từ khóa:");

        txtBookSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBookSearchKeyReleased(evt);
            }
        });

        cbBookSearchBy.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mã sách", "Tên sách", "Tác giả", "Thể loại" }));
        cbBookSearchBy.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnBookSearch.setText("Tìm kiếm");
        btnBookSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBookSearch.addActionListener(this::btnBookSearchActionPerformed);

        btnBookClear.setText("Hủy tìm");
        btnBookClear.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBookClear.addActionListener(this::btnBookClearActionPerformed);

        tbBook.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                        {"S0001", "Nhật ký của mẹ", "Nguyễn Nhật Ánh", "Truyện", "Cuốn sách Thay đổi cuộc sống với Nhân số học là tác phẩm được chị Lê Đỗ Quỳnh Hương phát triển từ tác phẩm gốc “The Complete Book of Numerology” của tiến sỹ David A. Phillips, khiến bộ môn Nhân số học khởi nguồn từ nhà toán học Pythagoras trở nên gần gũi, dễ hiểu hơn với độc giả Việt Nam.", "10"},
                        {"S0002", "Ngồi khóc trên cây", "Nguyễn Nhật Ánh", "Truyện", "Mở đầu là kỳ nghỉ hè tại một ngôi làng thơ mộng ven sông với nhân vật là những đứa trẻ mới lớn có vô vàn trò chơi đơn sơ hấp dẫn ghi dấu mãi trong lòng.   Mối tình đầu trong veo của cô bé Rùa và chàng sinh viên quê học ở thành phố có giống tình đầu của bạn thời đi học? Và cái cách họ thương nhau giấu giếm, không dám làm nhau buồn, khát khao hạnh phúc đến nghẹt thở có phải là câu chuyện chính?", "20"}
                },
                new String [] {
                        "Mã sách", "Tên sách", "Tác giả", "Thể loại", "Mô tả", "Số lượng còn lại"
                }
        ) {
            final Class<?>[] types = new Class [] {
                    java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            final boolean[] canEdit = new boolean [] {
                    false, false, false, false, false, false
            };

            public Class<?> getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbBook.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tbBook.getTableHeader().setReorderingAllowed(false);
        tbBook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbBookMouseClicked(evt);
            }
        });
        tbBook.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbBookKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tbBook);

        jLabel5.setText("Mã sách:");

        jLabel6.setText("Tên sách:");

        jLabel7.setText("Tác giả:");

        jLabel8.setText("Thể loại:");

        jLabel9.setText("Mô tả:");

        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 98, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 148, Short.MAX_VALUE)
        );

        txtBookCode.setText("S0001");

        txtBookName.setText("Nhật ký của mẹ");

        txtBookAuth.setText("Nguyễn Nhật Ánh");

        txtBookCategory.setText("Truyện");

        txtBookDescription.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txtBookDescription.setText("<html>Cuốn sách Thay đổi cuộc sống với Nhân số học là tác phẩm được chị Lê Đỗ Quỳnh Hương phát triển từ tác phẩm gốc “The Complete Book of Numerology” của tiến sỹ David A. Phillips, khiến bộ môn Nhân số học khởi nguồn từ nhà toán học Pythagoras trở nên gần gũi, dễ hiểu hơn với độc giả Việt Nam.</html>");
        txtBookDescription.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Menu"));

        txtBookAdd.setText("Thêm sách vào thư viện");
        txtBookAdd.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txtBookAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtBookAddMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtBookAddMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtBookAddMouseExited(evt);
            }
        });

        txtBookEdit.setText("Chỉnh sửa thông tin sách");
        txtBookEdit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txtBookEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtBookEditMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtBookEditMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtBookEditMouseExited(evt);
            }
        });

        txtBookDelete.setText("Xóa sách khỏi thư viện");
        txtBookDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txtBookDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtBookDeleteMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtBookDeleteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtBookDeleteMouseExited(evt);
            }
        });

        txtBookView.setText("Xem thông tin sách");
        txtBookView.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txtBookView.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtBookViewMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtBookViewMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtBookViewMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
                jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtBookAdd)
                                        .addComponent(txtBookDelete)
                                        .addComponent(txtBookView)
                                        .addComponent(txtBookEdit))
                                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
                jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(txtBookView)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtBookAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtBookEdit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtBookDelete)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane1)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addGap(17, 17, 17)
                                                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18, 18)
                                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jLabel5)
                                                                        .addComponent(jLabel6)
                                                                        .addComponent(jLabel7)
                                                                        .addComponent(jLabel8)
                                                                        .addComponent(jLabel9))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(txtBookCategory)
                                                                        .addComponent(txtBookAuth)
                                                                        .addComponent(txtBookName)
                                                                        .addComponent(txtBookCode)
                                                                        .addComponent(txtBookDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE))
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addComponent(jLabel1)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(txtBookSearch)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addComponent(cbBookSearchBy, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(btnBookSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(btnBookClear, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel5)
                                                        .addComponent(txtBookCode))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel6)
                                                        .addComponent(txtBookName))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel7)
                                                        .addComponent(txtBookAuth))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel8)
                                                        .addComponent(txtBookCategory))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel9)
                                                        .addComponent(txtBookDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(43, 43, 43)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnBookClear, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(cbBookSearchBy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(txtBookSearch)
                                                .addComponent(btnBookSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel1)))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))
        );

        jTabbedPane2.addTab("Quản lý sách", jPanel1);

        jLabel2.setText("Từ khóa:");

        txtStudentSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtStudentSearchKeyReleased(evt);
            }
        });

        cbStudentSearchBy.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mã đọc giả", "Họ và tên", "Số điện thoại", "Email" }));
        cbStudentSearchBy.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnStudentSearch.setText("Tìm kiếm");
        btnStudentSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnStudentSearch.addActionListener(this::btnStudentSearchActionPerformed);

        btnStudentClear.setText("Hủy tìm");
        btnStudentClear.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnStudentClear.addActionListener(this::btnStudentClearActionPerformed);

        tbStudent.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                        {"S0001", "Lê Nguyễn Hữu Tài", "0333333333", "main@huutai.com", "Kỹ thuật phần mềm"},
                        {"S0002", "Huỳnh Đình Mẫn", "0999999999", "man@gmail.com", "Kỹ thuật phần mềm"}
                },
                new String [] {
                        "Mã đọc giả", "Tên đọc giả", "Số điện thoại", "Email", "Chuyên ngành"
                }
        ) {
            final Class<?>[] types = new Class [] {
                    java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            final boolean[] canEdit = new boolean [] {
                    false, false, false, false, false
            };

            public Class<?> getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbStudent.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tbStudent.getTableHeader().setReorderingAllowed(false);
        tbStudent.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbStudentMouseClicked(evt);
            }
        });
        tbStudent.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbStudentKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tbStudent);

        jLabel10.setText("Mã đọc giả:");

        jLabel11.setText("Họ và tên:");

        jLabel12.setText("Số điện thoại:");

        jLabel13.setText("Email:");

        jLabel14.setText("Chuyên ngành:");

        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 98, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 148, Short.MAX_VALUE)
        );

        txtStudentCode.setText("S0001");

        txtStudentName.setText("Lê Nguyễn Hữu Tài");

        txtStudentPhone.setText("0336272723");

        txtStudentEmail.setText("main.huutai2101@gmail.com");

        txtStudentMajor.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txtStudentMajor.setText("Kỹ thuật phần mềm");
        txtStudentMajor.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Menu"));

        txtStudentAdd.setText("Thêm đọc giả");
        txtStudentAdd.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txtStudentAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtStudentAddMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtStudentAddMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtStudentAddMouseExited(evt);
            }
        });

        txtStudentEdit.setText("Chỉnh sửa thông tin đọc giả");
        txtStudentEdit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txtStudentEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtStudentEditMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtStudentEditMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtStudentEditMouseExited(evt);
            }
        });

        txtStudentDelete.setText("Xóa thông tin đọc giả");
        txtStudentDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txtStudentDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtStudentDeleteMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtStudentDeleteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtStudentDeleteMouseExited(evt);
            }
        });

        txtStudentView.setText("Xem thông tin đọc giả");
        txtStudentView.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txtStudentView.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtStudentViewMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtStudentViewMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtStudentViewMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
                jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel7Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtStudentAdd)
                                        .addComponent(txtStudentDelete)
                                        .addComponent(txtStudentView)
                                        .addComponent(txtStudentEdit))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
                jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel7Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(txtStudentView)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtStudentAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtStudentEdit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtStudentDelete)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
                jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel5Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane2)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(jPanel5Layout.createSequentialGroup()
                                                                .addGap(17, 17, 17)
                                                                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18, 18)
                                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jLabel10)
                                                                        .addComponent(jLabel11)
                                                                        .addComponent(jLabel12)
                                                                        .addComponent(jLabel13)
                                                                        .addComponent(jLabel14))
                                                                .addGap(32, 32, 32)
                                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(txtStudentEmail)
                                                                        .addComponent(txtStudentPhone)
                                                                        .addComponent(txtStudentName)
                                                                        .addComponent(txtStudentCode)
                                                                        .addComponent(txtStudentMajor, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                        .addGroup(jPanel5Layout.createSequentialGroup()
                                                                .addComponent(jLabel2)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(txtStudentSearch)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanel5Layout.createSequentialGroup()
                                                                .addComponent(cbStudentSearchBy, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(btnStudentSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(btnStudentClear, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
                jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel5Layout.createSequentialGroup()
                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel10)
                                                        .addComponent(txtStudentCode))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel11)
                                                        .addComponent(txtStudentName))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel12)
                                                        .addComponent(txtStudentPhone))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel13)
                                                        .addComponent(txtStudentEmail))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel14)
                                                        .addComponent(txtStudentMajor, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(43, 43, 43)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnStudentClear, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(cbStudentSearchBy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(txtStudentSearch)
                                                .addComponent(btnStudentSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel2)))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))
        );

        jTabbedPane2.addTab("Quản lý đọc giả", jPanel5);

        jLabel15.setText("Từ khóa:");

        txtLoanSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtLoanSearchKeyReleased(evt);
            }
        });

        cbLoanSearch.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mã đọc giả", "Tên đọc giả" }));
        cbLoanSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnLoanClear.setText("Hủy tìm");
        btnLoanClear.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLoanClear.addActionListener(this::btnLoanClearActionPerformed);

        btnLoanSearch.setText("Tìm kiếm");
        btnLoanSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLoanSearch.addActionListener(this::btnLoanSearchActionPerformed);

        jLabel16.setText("Đọc giả");

        tbLoanStudent.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                        {"S0001", "Lê Nguyễn Hữu Tài"},
                        {"S0002", "Huỳnh Đình Mẫn"}
                },
                new String [] {
                        "Mã đọc giả", "Tên đọc giả"
                }
        ) {
            final Class<?>[] types = new Class [] {
                    java.lang.String.class, java.lang.String.class
            };
            final boolean[] canEdit = new boolean [] {
                    false, false
            };

            public Class<?> getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbLoanStudent.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tbLoanStudent.getTableHeader().setReorderingAllowed(false);
        tbLoanStudent.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbLoanStudentMouseClicked(evt);
            }
        });
        tbLoanStudent.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbLoanStudentKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(tbLoanStudent);

        jLabel17.setText("Sách mượn");

        tbLoanBook.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                        {"S0001", "Lê Nguyễn Hữu Tài", null, null, null, null},
                        {"S0002", "Huỳnh Đình Mẫn", null, null, null, null}
                },
                new String [] {
                        "Mã sách", "Tên sách", "Ngày mượn sách", "Hạn trả sách", "Ngày trả sách", "Người duyệt"
                }
        ) {
            final Class<?>[] types = new Class [] {
                    java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class
            };
            final boolean[] canEdit = new boolean [] {
                    false, false, false, false, false, false
            };

            public Class<?> getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbLoanBook.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tbLoanBook.getTableHeader().setReorderingAllowed(false);
        tbLoanBook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbLoanBookMouseClicked(evt);
            }
        });
        tbLoanBook.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbLoanBookKeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(tbLoanBook);

        btnLoanBorrow.setText("Mượn sách");
        btnLoanBorrow.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLoanBorrow.addActionListener(this::btnLoanBorrowActionPerformed);

        btnLoanReturn.setText("Trả sách");
        btnLoanReturn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLoanReturn.addActionListener(this::btnLoanReturnActionPerformed);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(jLabel15)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(txtLoanSearch)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(cbLoanSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(btnLoanSearch)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(btnLoanClear))
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel16)
                                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 573, Short.MAX_VALUE)
                                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                                .addComponent(jLabel17)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(btnLoanBorrow, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(btnLoanReturn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel15)
                                        .addComponent(txtLoanSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(cbLoanSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnLoanClear)
                                        .addComponent(btnLoanSearch))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGap(53, 53, 53)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel16)
                                                        .addComponent(jLabel17)))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(btnLoanBorrow)
                                                        .addComponent(btnLoanReturn))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane3)
                                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE))
                                .addContainerGap())
        );

        jTabbedPane2.addTab("Quản lý mượn trả sách", jPanel2);

        jLabel3.setFont(new java.awt.Font("Tahoma", Font.PLAIN, 24)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("THƯ VIỆN ĐẠI HỌC KHOA HỌC");

        jLabel4.setText("Xin chào,");

        txtWelcomeName.setText("Lê Nguyễn Hữu Tài");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jTabbedPane2)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel4)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtWelcomeName)
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4)
                                        .addComponent(txtWelcomeName))
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 587, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>

    private void txtStudentViewMouseExited(java.awt.event.MouseEvent evt) {
        txtStudentView.setForeground(Color.BLACK);
    }

    private void txtStudentViewMouseEntered(java.awt.event.MouseEvent evt) {
        txtStudentView.setForeground(Color.BLUE);
    }

    private void txtStudentViewMouseClicked(java.awt.event.MouseEvent evt) {
        viewStudent();
    }

    private void txtStudentDeleteMouseExited(java.awt.event.MouseEvent evt) {
        txtStudentDelete.setForeground(Color.BLACK);
    }

    private void txtStudentDeleteMouseEntered(java.awt.event.MouseEvent evt) {
        txtStudentDelete.setForeground(Color.BLUE);
    }

    private void txtStudentDeleteMouseClicked(java.awt.event.MouseEvent evt) {
        deleteStudent();
    }

    private void txtStudentEditMouseExited(java.awt.event.MouseEvent evt) {
        txtStudentEdit.setForeground(Color.BLACK);
    }

    private void txtStudentEditMouseEntered(java.awt.event.MouseEvent evt) {
        txtStudentEdit.setForeground(Color.BLUE);
    }

    private void txtStudentEditMouseClicked(java.awt.event.MouseEvent evt) {
        editStudent();
    }

    private void txtStudentAddMouseExited(java.awt.event.MouseEvent evt) {
        txtStudentAdd.setForeground(Color.BLACK);
    }

    private void txtStudentAddMouseEntered(java.awt.event.MouseEvent evt) {
        txtStudentAdd.setForeground(Color.BLUE);
    }

    private void txtStudentAddMouseClicked(java.awt.event.MouseEvent evt) {
        addStudent();
    }

    private void tbStudentKeyPressed(java.awt.event.KeyEvent evt) {
        int selectedRow = tbStudent.getSelectedRow();
        switch(evt.getKeyCode()) {
            case KeyEvent.VK_DOWN:
                if (selectedRow < tbStudent.getRowCount() - 1) {
                    setStudentInfo(selectedRow + 1);
                }
                break;
            case KeyEvent.VK_UP:
                if (selectedRow > 0) {
                    setStudentInfo(selectedRow + -1);
                }
                break;
            case KeyEvent.VK_ENTER:
                if (selectedRow == tbStudent.getRowCount() - 1) {
                    selectedRow = -1;
                }
                setStudentInfo(selectedRow + 1);
                break;
        }
    }

    private void tbStudentMouseClicked(java.awt.event.MouseEvent evt) {
        int selectedRow = tbStudent.getSelectedRow();
        setStudentInfo(selectedRow);
    }

    private void btnStudentClearActionPerformed(java.awt.event.ActionEvent evt) {
        txtStudentSearch.setText("");
        searchStudent(false);
    }

    private void btnStudentSearchActionPerformed(java.awt.event.ActionEvent evt) {
        searchStudent(false);
    }

    private void txtStudentSearchKeyReleased(java.awt.event.KeyEvent evt) {
        searchStudent(false);
    }

    private void txtBookViewMouseExited(java.awt.event.MouseEvent evt) {
        txtBookView.setForeground(Color.BLACK);
    }

    private void txtBookViewMouseEntered(java.awt.event.MouseEvent evt) {
        txtBookView.setForeground(Color.BLUE);
    }

    private void txtBookViewMouseClicked(java.awt.event.MouseEvent evt) {
        viewBook();
    }

    private void txtBookDeleteMouseExited(java.awt.event.MouseEvent evt) {
        txtBookDelete.setForeground(Color.BLACK);
    }

    private void txtBookDeleteMouseEntered(java.awt.event.MouseEvent evt) {
        txtBookDelete.setForeground(Color.BLUE);
    }

    private void txtBookDeleteMouseClicked(java.awt.event.MouseEvent evt) {
        deleteBook();
    }

    private void txtBookEditMouseExited(java.awt.event.MouseEvent evt) {
        txtBookEdit.setForeground(Color.BLACK);
    }

    private void txtBookEditMouseEntered(java.awt.event.MouseEvent evt) {
        txtBookEdit.setForeground(Color.BLUE);
    }

    private void txtBookEditMouseClicked(java.awt.event.MouseEvent evt) {
        editBook();
    }

    private void txtBookAddMouseExited(java.awt.event.MouseEvent evt) {
        txtBookAdd.setForeground(Color.BLACK);
    }

    private void txtBookAddMouseEntered(java.awt.event.MouseEvent evt) {
        txtBookAdd.setForeground(Color.BLUE);
    }

    private void txtBookAddMouseClicked(java.awt.event.MouseEvent evt) {
        addBook();
    }

    private void tbBookKeyPressed(java.awt.event.KeyEvent evt) {
        int selectedRow = tbBook.getSelectedRow();
        switch(evt.getKeyCode()) {
            case KeyEvent.VK_DOWN:
                if (selectedRow < tbBook.getRowCount() - 1) {
                    setBookInfo(selectedRow + 1);
                }
                break;
            case KeyEvent.VK_UP:
                if (selectedRow > 0) {
                    setBookInfo(selectedRow + -1);
                }
                break;
            case KeyEvent.VK_ENTER:
                if (selectedRow == tbBook.getRowCount() - 1) {
                    selectedRow = -1;
                }
                setBookInfo(selectedRow + 1);
                break;
        }
    }

    private void tbBookMouseClicked(java.awt.event.MouseEvent evt) {
        int selectedRow = tbBook.getSelectedRow();
        setBookInfo(selectedRow);
    }

    private void btnBookClearActionPerformed(java.awt.event.ActionEvent evt) {
        txtBookSearch.setText("");
        searchBook();
    }

    private void btnBookSearchActionPerformed(java.awt.event.ActionEvent evt) {
        searchBook();
    }

    private void txtBookSearchKeyReleased(java.awt.event.KeyEvent evt) {
        searchBook();
    }

    private void tbLoanStudentMouseClicked(java.awt.event.MouseEvent evt) {
        int selectedRow = tbLoanStudent.getSelectedRow();
        getBookLoan(selectedRow);
    }

    private void tbLoanStudentKeyPressed(java.awt.event.KeyEvent evt) {
        int selectedRow = tbLoanStudent.getSelectedRow();
        switch(evt.getKeyCode()) {
            case KeyEvent.VK_DOWN:
                if (selectedRow < tbLoanStudent.getRowCount() - 1) {
                    getBookLoan(selectedRow + 1);
                }
                break;
            case KeyEvent.VK_UP:
                if (selectedRow > 0) {
                    getBookLoan(selectedRow + -1);
                }
                break;
            case KeyEvent.VK_ENTER:
                if (selectedRow == tbLoanStudent.getRowCount() - 1) {
                    selectedRow = -1;
                }
                getBookLoan(selectedRow + 1);
                break;
        }
    }

    private void tbLoanBookMouseClicked(java.awt.event.MouseEvent evt) {
    }

    private void tbLoanBookKeyPressed(java.awt.event.KeyEvent evt) {
    }

    private void btnLoanBorrowActionPerformed(java.awt.event.ActionEvent evt) {
        loanBorrow();
    }

    private void btnLoanReturnActionPerformed(java.awt.event.ActionEvent evt) {
        loanReturn();
    }

    private void btnLoanSearchActionPerformed(java.awt.event.ActionEvent evt) {
            searchStudent(true);
    }

    private void btnLoanClearActionPerformed(java.awt.event.ActionEvent evt) {
        txtLoanSearch.setText("");
        searchStudent(true);
    }

    private void txtLoanSearchKeyReleased(java.awt.event.KeyEvent evt) {
        searchStudent(true);
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton btnBookClear;
    private javax.swing.JButton btnBookSearch;
    private javax.swing.JButton btnLoanClear;
    private javax.swing.JButton btnLoanSearch;
    private javax.swing.JButton btnStudentClear;
    private javax.swing.JButton btnStudentSearch;
    private javax.swing.JComboBox<String> cbBookSearchBy;
    private javax.swing.JComboBox<String> cbLoanSearch;
    private javax.swing.JComboBox<String> cbStudentSearchBy;
    private javax.swing.JButton btnLoanBorrow;
    private javax.swing.JButton btnLoanReturn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable tbBook;
    private javax.swing.JTable tbLoanBook;
    private javax.swing.JTable tbLoanStudent;
    private javax.swing.JTable tbStudent;
    private javax.swing.JLabel txtBookAdd;
    private javax.swing.JLabel txtBookAuth;
    private javax.swing.JLabel txtBookCategory;
    private javax.swing.JLabel txtBookCode;
    private javax.swing.JLabel txtBookDelete;
    private javax.swing.JLabel txtBookDescription;
    private javax.swing.JLabel txtBookEdit;
    private javax.swing.JLabel txtBookName;
    private javax.swing.JTextField txtBookSearch;
    private javax.swing.JLabel txtBookView;
    private javax.swing.JTextField txtLoanSearch;
    private javax.swing.JLabel txtStudentAdd;
    private javax.swing.JLabel txtStudentCode;
    private javax.swing.JLabel txtStudentDelete;
    private javax.swing.JLabel txtStudentEdit;
    private javax.swing.JLabel txtStudentEmail;
    private javax.swing.JLabel txtStudentMajor;
    private javax.swing.JLabel txtStudentName;
    private javax.swing.JLabel txtStudentPhone;
    private javax.swing.JTextField txtStudentSearch;
    private javax.swing.JLabel txtStudentView;
    private javax.swing.JLabel txtWelcomeName;
    // End of variables declaration                   
}
