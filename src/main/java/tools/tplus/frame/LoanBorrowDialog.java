package tools.tplus.frame;

import lombok.Getter;
import tools.tplus.model.*;
import tools.tplus.service.BookListService;
import tools.tplus.service.BookLoanService;
import tools.tplus.service.StudentService;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LoanBorrowDialog extends javax.swing.JDialog {

    public static final int OK = 1;
    public static final int CANCEL = 0;

    private final Librarian librarian;
    private final String studentCode;

    private int code = 0;
    private Book book;

    @Getter
    private BookLoan bookLoan;
    private Student student;

    public LoanBorrowDialog(Frame parent, boolean modal, Librarian librarian, String studentCode) {
        super(parent, modal);
        this.librarian = librarian;
        this.studentCode = studentCode;
        initComponents();
        init();
    }

    public int getCode() {
        return code;
    }

    private void init() {
        student = StudentService.getByCode(studentCode);
        assert student != null;

        txtNotity.setText("");
        txtStudentCode.setText(student.getCode());
        txtStudentName.setText(student.getName());
        txtLibriranName.setText(librarian.getName());
    }

    private void findBook() {
        String code = txtBookCode.getText();
        BookList bookList = BookListService.getByCode(code);
        if (bookList == null) {
            txtBookName.setText("Sách không tồn tại");
            return;
        }

        if (bookList.getAmount() == 0) {
            txtBookName.setText("Sách đã hết");
            return;
        }

        book = bookList.getBook();
        txtBookName.setText(book.getCode() + " - " + book.getTitle());
    }

    private boolean validateData() {
        if (book == null) {
            txtNotity.setText("Chưa có thông tin sách!");
            return false;
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        try {
            LocalDate localDate = LocalDate.parse(txtLoanDate.getText(), formatter);
        } catch (Exception e) {
            txtNotity.setText("Ngày mượn không hợp lệ!");
            return false;
        }

        return true;
    }

    private void save() {
        String[] date = txtLoanDate.getText().split("/");

        bookLoan = new BookLoan();
        bookLoan.setBook(book);
        bookLoan.setLoanDate(LocalDate.of(Integer.parseInt(date[2]), Integer.parseInt(date[1]),
                Integer.parseInt(date[0])));
        bookLoan.setLoanDays(Integer.parseInt(txtAmount.getText()));
        bookLoan.setLibrarian(librarian);
        student.getBookLoans().add(bookLoan);

        BookLoanService.saveOrUpdate(bookLoan);
        StudentService.saveOrUpdate(student);

        BookList bookList = BookListService.getByCode(book.getCode());
        assert bookList != null;
        bookList.setAmount(bookList.getAmount() - 1);
        BookListService.saveOrUpdate(bookList);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnCancel = new javax.swing.JButton();
        btnOk = new javax.swing.JButton();
        txtStudentName = new javax.swing.JTextField();
        txtStudentCode = new javax.swing.JTextField();
        txtBookCode = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtBookName = new javax.swing.JTextField();
        btnFind = new javax.swing.JButton();
        txtLoanDate = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtAmount = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtReturnDate = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtLibriranName = new javax.swing.JTextField();
        txtNotity = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Mượn sách");
        setResizable(false);

        jLabel5.setText("Mã đọc giả");

        jLabel6.setText("Tên đọc giả");

        jLabel7.setText("Mã sách");

        btnCancel.setText("Hủy");
        btnCancel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCancel.addActionListener(this::btnCancelActionPerformed);

        btnOk.setText("OK");
        btnOk.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnOk.addActionListener(this::btnOkActionPerformed);

        txtStudentName.setEditable(false);

        txtStudentCode.setEditable(false);

        txtBookCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBookCodeKeyReleased(evt);
            }
        });

        jLabel1.setText("Tên sách");

        txtBookName.setEditable(false);

        btnFind.setText("Tìm");
        btnFind.addActionListener(this::btnFindActionPerformed);

        jLabel2.setText("Ngày mượn");

        jLabel10.setText("Số ngày mượn");

        jLabel3.setText("Hạn trả sách");

        txtReturnDate.setEditable(false);

        jLabel4.setText("Người duyệt");

        txtLibriranName.setEditable(false);

        txtNotity.setForeground(new java.awt.Color(255, 0, 0));
        txtNotity.setText("Không tìm thấy sách!");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 176, Short.MAX_VALUE)
                        .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel1)
                            .addComponent(jLabel5))
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtStudentCode)
                            .addComponent(txtBookName)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtBookCode)
                                .addGap(5, 5, 5)
                                .addComponent(btnFind))
                            .addComponent(txtStudentName)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel10)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtNotity)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtLibriranName)
                            .addComponent(txtReturnDate)
                            .addComponent(txtAmount)
                            .addComponent(txtLoanDate))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtStudentCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtStudentName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtBookCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFind))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtBookName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtLoanDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtReturnDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtLibriranName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtNotity)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel)
                    .addComponent(btnOk))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>                        

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {
        code = CANCEL;
        this.dispose();
    }                                         

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {
        if (validateData()) {
            save();
            code = OK;
            this.dispose();
        }
    }

    private void btnFindActionPerformed(java.awt.event.ActionEvent evt) {                                        
        findBook();
    }                                       

    private void txtBookCodeKeyReleased(java.awt.event.KeyEvent evt) {                                        
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            findBook();
        }
    }                                       


    // Variables declaration - do not modify                     
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnFind;
    private javax.swing.JButton btnOk;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField txtAmount;
    private javax.swing.JTextField txtBookCode;
    private javax.swing.JTextField txtBookName;
    private javax.swing.JTextField txtLibriranName;
    private javax.swing.JTextField txtLoanDate;
    private javax.swing.JLabel txtNotity;
    private javax.swing.JTextField txtReturnDate;
    private javax.swing.JTextField txtStudentCode;
    private javax.swing.JTextField txtStudentName;
    // End of variables declaration                   
}
