package tools.tplus.frame;

public class InfoDialog extends javax.swing.JDialog {

    public InfoDialog(java.awt.Frame parent, boolean modal, String msg) {
        super(parent, modal);
        initComponents();
        txtMsg.setText("<html>" + msg + "</html>");
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        txtMsg = new javax.swing.JLabel();
        btnYes = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Thông tin");
        setResizable(false);

        txtMsg.setText("<html>Không thể xóa sách vì có một số đọc giả chưa trả sách!</html>");
        txtMsg.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        btnYes.setText("OK");
        btnYes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnYes.addActionListener(this::btnYesActionPerformed);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnYes, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtMsg, javax.swing.GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtMsg, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnYes)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>                        

    private void btnYesActionPerformed(java.awt.event.ActionEvent evt) {                                       
        this.dispose();
    }                                      

    // Variables declaration - do not modify                     
    private javax.swing.JButton btnYes;
    private javax.swing.JLabel txtMsg;
    // End of variables declaration                   
}
