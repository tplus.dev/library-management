-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2021 at 01:51 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library-management`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
                        `id` bigint(20) NOT NULL,
                        `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                        `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                        `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
                        `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                        `category_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `author`, `code`, `description`, `title`, `category_id`) VALUES
(9, 'Chris Ferrie, Cara Florance', 'B0001', 'Đây là 9 cuốn trong bộ sách Vỡ lòng về Khoa học của tác giả Chris Ferrie đã được nhiều quốc gia đón nhận (những chủ đề tiếp theo sẽ ra mắt trong thời gian tới). Khi quan điểm giáo dục sớm cũng như xu hướng STEAM (khoa học – công nghệ – kĩ thuật – nghệ thuật – toán học) đang ngày càng chứng minh sự hiệu quả đối với học sinh thì bộ sách vỡ lòng về khoa học này chính là một trong những nguồn tài liệu tham khảo mà trẻ em cần. Những cuốn sách nhanh chóng đạt xếp hạng cao nhờ đáp ứng rất tốt các tiêu chí: nội dung chuẩn xác, cô đọng, minh họa “trực quan”, sáng rõ – phù hợp với khả năng tiếp thu của các em từ tuổi 4+.', 'Sách Vỡ Lòng Về Khoa Học', 8),
(10, 'HappyLive', 'B0002', 'Mọi người hay nói vui với nhau rằng: Chẳng ai chết đuối trong mồ hôi của chính mình mà chỉ chết chìm trong sự lười biếng. Mồ hôi là phép ẩn dụ cho cố gắng của bạn khi làm việc. Tuy nhiên, mồ hôi đổ ra khi bạn chạy 100m và 10km rất khác nhau, bạn bỏ ra công sức càng nhiều, thành quả đạt được càng rõ rệt. Nhưng dù mồ hôi có đổ nhiều thế nào đi nữa, nó vẫn không thể nhấn chìm bạn. Lười biếng thì ngược lại, bạn càng buông xuôi mình thì bản thân càng dễ lún sâu vào vũng lầy lười nhác, lâu dần, bạn trở thành người sống tùy ý, không có ý chí và nghị lực để làm bất cứ việc gì.', '66 Ngày Thử Thách', 3),
(11, 'William B. Irvine', 'B0003', 'Bạn mong muốn điều gì từ cuộc sống này? Có thể câu trả lời của bạn là muốn có một người bạn đời biết quan tâm, một công việc tốt và một ngôi nhà đẹp, nhưng chúng thực ra chỉ là một số thứ bạn muốn có trong cuộc sống. Khi hỏi bạn mong muốn điều gì từ cuộc sống này, tôi đang hỏi theo nghĩa rộng nhất. Tôi không hỏi về những mục tiêu mà bạn đề ra khi thực hiện các hoạt động hằng ngày, mà tôi đang hỏi về mục tiêu lớn lao trong cuộc sống của bạn. Nói cách khác, trong số những thứ bạn có thể theo đuổi trong cuộc sống, thứ nào bạn tin là có giá trị nhất?', 'Phong Cách Sống Bản Lĩnh Và Bình Thản', 3),
(12, 'Dương Duy Bách', 'B0004', 'Làm chủ tuổi 20 được viết lại dựa trên những trải nghiệm trong cuộc sống của tác giả Dương Duy Bách – một người sớm tự lập và đạt được nhiều thành công ở tuổi 20. Ngoài câu chuyện của chính mình, tác giả còn ghi lại những bài học mà anh học được từ những người trẻ thành công khác và phân tích lý do khách quan khiến họ đạt được mục tiêu khi tuổi đời còn rất trẻ.', 'Làm Chủ Tuổi 20', 3),
(13, 'Trí Việt', 'B0005', 'Muôn Kiếp Nhân Sinh cung cấp cho bạn đọc kho kiến thức mới mẻ, vô tận của nhân loại lần đầu được hé mở, cùng những phân tích uyên bác, tiên đoán bất ngờ về hiện tại và tương lai thế giới của những bậc hiền triết thông thái.', 'Muôn Kiếp Nhân Sinh', 6),
(14, 'Simon Tudhope', 'B0006', '3-8 tuổi là giai đoạn bộ não của trẻ phát triển nhanh nhất để có thể nhận thức được màu sắc, hình khối, đồ vật, phát triển mạnh mẽ về cả ngôn ngữ, tư duy logic và khả năng sáng tạ Cũng bởi thế mà trẻ cần có những \"công cụ\" phù hợp để có thể nâng cao được toàn diện những kỹ năng của mình.', 'Dán Hình Đầu Đời Cho Bé - Ô Tô', 8),
(15, 'Nhã Nam', 'B0007', 'Đúng là trẻ nhỏ chưa cần “đọc” sách, mà các con cần được “chơi” với sách như là một món đồ chơi giàu tính tương tác. Sách cho lứa tuổi dưới 3 thường phải đảm bảo các tiêu chí: giấy dày để trẻ khó xé hay cắn hỏng, hình ảnh rất to rõ và thường ít màu để phù hợp với khả năng tiếp thu của trẻ, có hiệu ứng tương tác như lật giở-sờ chạm… để trẻ được vận động tay chân với sách, in bằng màu tốt và phủ bóng để đảm bảo an toàn nếu trẻ lỡ cắn hay liếm sách, nội dung thật gần gũi với nhu cầu và tâm lý lứa tuổi để bố mẹ dễ nói chuyện thủ thỉ với trẻ nhằm tăng cường giao tiếp thân mật…', 'Song Ngữ 0-3 Tuổi: Mẹ Đâu Rồi?', 8),
(16, 'Nhã Nam', 'B0008', 'Cậu thỏ con chẳng ngủ được, vì cậu sợ vô số thứ và vô số tiếng động trong phòng. Đôi mắt đáng sợ kia của ai? Hình ảnh thấp thoáng kia là cái gì? Tiếng động ù ù nào đáng hãi thế? Thỏ mẹ làm thế nào để trấn an cậu nhỉ? Chúng mình cùng xem thỏ mẹ có tuyệt chiêu gì giúp con!', 'Song Ngữ 0-3 Tuổi: Chúc Ngủ Ngon', 8),
(17, 'Jess M. Brallier, Robert Andrew Parker', 'B0009', 'Marie Curie chắc chắn là cái tên nổi tiếng và rất quen thuộc với chúng ta. Thậm chí ở Việt Nam cũng có trường học mang tên bà. Nhưng có ai biết rằng, bà đã từng có một tuổi thơ rất nghèo khó và phải làm người làm thuê cho gia đình khác. Nhưng nếu chỉ vì thế thì lý do gì mà một cô bé làm thuê lại được cả thế giới tôn vinh như vậy?', 'Albert Einstein Là Ai?', 3),
(18, 'Trần Thanh Hương', 'B0010', 'Cánh cửa dẫn đến thành công trong sự nghiệp hay cuộc sống cũng giống như lối đi dẫn vào câu lạc bộ đêm. Chúng ta có Cánh cửa thứ nhất: cửa chính, nơi hàng dài người xếp hàng; 99% trong số đó chờ đợi và hy vọng người tiếp theo là mình. Một thiểu số khác thì đi Cánh cửa thứ hai: cửa VIP, cánh cửa dành cho các tỷ phú, người nổi tiếng và những người sinh ra trong thế giới đó.', 'Kẻ Khôn Đi Lối Khác', 6),
(19, 'GIÁC NGỘ', 'B0011', 'Mở ra một thế giới vạn hoa với những câu truyện ngụ ngôn được tạo nên bởi nhiều nhân vật huyền thoại và các loài vật kỳ thú. Mỗi trang là một bức tranh vẽ diễn sinh động diễn tả một câu truyện ý nghĩa và sâu sắc.', 'SỐNG TỬ TẾ - NUÔI DƯỠNG NHÂN CÁCH SỐNG', 6),
(20, 'Nguyên Hồng', 'B0012', 'Người ta hay giấu giếm và che đậy sự thật, nhất là sự đáng buồn trong gia đình. Có lợi ích gì không? Những ngày thơ ấu mà Nguyên Hồng kể lại, tôi không muốn biết là có nên hay không, tôi chỉ thấy trong những kỉ niệm đau đớn ấy rung động cực điểm của một linh hồn trẻ dại, lạc loài trong những lề lối khắc nghiệt của một gia đình sắp tàn.', 'Những Ngày Thơ Ấu', 6),
(21, 'Albert Rutherford', 'B0013', 'Như bạn có thể thấy, chìa khóa để trở thành một người có tư duy phản biện tốt chính là sự tự nhận thức. Bạn cần phải đánh giá trung thực những điều trước đây bạn nghĩ là đúng, cũng như quá trình suy nghĩ đã dẫn bạn tới những kết luận đó. Nếu bạn không có những lý lẽ hợp lý, hoặc nếu suy nghĩ của bạn bị ảnh hưởng bởi những kinh nghiệm và cảm xúc, thì lúc đó hãy cân nhắc sử dụng tư duy phản biện! Bạn cần phải nhận ra được rằng con người, kể từ khi sinh ra, rất giỏi việc đưa ra những lý do lý giải cho những suy nghĩ khiếm khuyết của mình. Nếu bạn đang có những kết luận sai lệch này thì có một sự thật là những đức tin của bạn thường mâu thuẫn với nhau và đó thường là kết quả của thiên kiến xác nhận, nhưng nếu bạn biết điều này, thì bạn đã tiến gần hơn tới sự thật rồi!', 'Tư Duy Phản Biện', 3),
(22, 'Wabooks', 'B0014', 'Đây thực sự là một cú hit lớn trong ngành Ehon Nhật Bản. Vậy cuốn ehon có điều gì mà có thể khiến các em bé ngừng khóc? - Đầu tiên, đó là hình ảnh minh họa. Em bé thường chú ý tới những hình vẽ có màu sắc rực rỡ, tươi sáng và hình dạng tương tự như khuôn mặt hay đôi mắt của con người. “Nhân vật chính” của Moi Moi đáp ứng được những điều kiện đó, và là hình minh họa được chính các em bé lựa chọn thông qua những cuộc thực nghiệm.', 'Ehon Nhật Bản cho trẻ sơ sinh', 8),
(23, 'Nguyễn Nhật Ánh', 'B0015', 'Tôi gửi tình yêu cho mùa hè, nhưng mùa hè không giữ nổi. Mùa hè chỉ biết ra hoa, phượng đỏ sân trường và tiếng ve nỉ non trong lá. Mùa hè ngây ngô, giống như tôi vậy. Nó chẳng làm được những điều tôi ký thác. Nó để Hà Lan đốt tôi, đốt rụi. Trái tim tôi cháy thành tro, rơi vãi trên đường về.', 'Mắt Biếc (Tái Bản 2019)', 6),
(24, 'Nguyễn Nhật Ánh', 'B0016', 'Không giống như những tác phẩm trước đây lấy bối cảnh vùng quê miền Trung đầy ắp những hoài niệm tuổi thơ dung dị, trong trẻo với các nhân vật ở độ tuổi dậy thì, trong quyển sách mới lần này nhà văn Nguyễn Nhật Ánh lấy bối cảnh chính là Sài Gòn – Thành phố Hồ Chí Minh nơi tác giả sinh sống (như là một sự đền đáp ân tình với mảnh đất miền Nam).', 'Con Chim Xanh Biếc Bay Về', 6),
(25, 'Nguyễn Nhật Ánh', 'B0017', '(Không có mô tả)', 'Tôi Thấy Hoa Vàng Trên Cỏ Xanh', 6),
(26, 'Nguyễn Nhật Ánh', 'B0018', '(Không có mô tả)', 'Cho Tôi Xin Một Vé Đi Tuổi Thơ', 6),
(27, 'Nguyễn Nhật Ánh', 'B0019', '(Không có mô tả)', 'Ngồi Khóc Trên Cây', 6),
(28, 'Nguyễn Nhật Ánh', 'B0020', '(Không có mô tả)', 'Làm Bạn Với Bầu Trời', 6);

-- --------------------------------------------------------

--
-- Table structure for table `booklist`
--

CREATE TABLE `booklist` (
                            `id` bigint(20) NOT NULL,
                            `amount` int(11) DEFAULT NULL,
                            `book_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `booklist`
--

INSERT INTO `booklist` (`id`, `amount`, `book_id`) VALUES
(29, 130, 9),
(30, 157, 10),
(31, 126, 11),
(32, 1, 12),
(33, 21, 13),
(34, 61, 14),
(35, 89, 15),
(36, 67, 16),
(37, 80, 17),
(38, 257, 18),
(39, 107, 19),
(40, 255, 20),
(41, 281, 21),
(42, 21, 22),
(43, 215, 23),
(44, 207, 24),
(45, 125, 25),
(46, 253, 26),
(47, 20, 27),
(48, 230, 28);

-- --------------------------------------------------------

--
-- Table structure for table `bookloan`
--

CREATE TABLE `bookloan` (
                            `id` bigint(20) NOT NULL,
                            `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                            `loanDate` date DEFAULT NULL,
                            `loanDays` int(11) DEFAULT NULL,
                            `returnDate` date DEFAULT NULL,
                            `book_id` bigint(20) DEFAULT NULL,
                            `librarian_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookloan`
--

INSERT INTO `bookloan` (`id`, `code`, `loanDate`, `loanDays`, `returnDate`, `book_id`, `librarian_id`) VALUES
(60, 'T0001', '2021-03-02', 50, NULL, 19, 55),
(61, 'T0002', '2021-03-01', 52, '2021-03-21', 9, 53),
(62, 'T0003', '2021-03-02', 22, NULL, 9, 51),
(63, 'T0004', '2021-03-02', 26, NULL, 15, 58),
(64, 'T0005', '2021-03-03', 37, NULL, 20, 49),
(65, 'T0006', '2021-03-14', 41, '2021-05-05', 18, 49),
(66, 'T0007', '2021-03-05', 23, NULL, 19, 55),
(67, 'T0008', '2021-03-03', 58, NULL, 15, 52),
(68, 'T0009', '2021-03-06', 47, NULL, 17, 51),
(69, 'T0010', '2021-03-07', 35, '2021-05-03', 18, 55),
(70, 'T0011', '2021-03-14', 44, NULL, 20, 56),
(71, 'T0012', '2021-03-06', 31, '2021-04-27', 18, 53),
(72, 'T0013', '2021-03-04', 50, '2021-04-11', 18, 57),
(73, 'T0014', '2021-03-14', 23, '2021-04-13', 15, 56),
(74, 'T0015', '2021-03-14', 58, '2021-04-09', 21, 50),
(75, 'T0016', '2021-03-06', 55, '2021-04-18', 15, 49),
(76, 'T0017', '2021-03-12', 41, '2021-04-19', 14, 52),
(77, 'T0018', '2021-03-08', 31, NULL, 12, 57),
(78, 'T0019', '2021-03-06', 40, '2021-04-25', 16, 49),
(79, 'T0020', '2021-03-12', 54, '2021-04-08', 15, 51),
(80, 'T0021', '2021-03-01', 33, NULL, 9, 52),
(81, 'T0022', '2021-03-03', 24, NULL, 17, 51),
(82, 'T0023', '2021-03-03', 55, '2021-05-01', 12, 52),
(83, 'T0024', '2021-03-10', 31, NULL, 20, 52),
(84, 'T0025', '2021-03-09', 42, '2021-04-29', 13, 51),
(85, 'T0026', '2021-03-08', 21, NULL, 12, 51),
(86, 'T0027', '2021-03-11', 20, NULL, 21, 57),
(87, 'T0028', '2021-03-06', 20, '2021-05-04', 17, 56),
(88, 'T0029', '2021-03-06', 37, '2021-04-07', 22, 49),
(89, 'T0030', '2021-03-08', 59, NULL, 9, 51),
(90, 'T0031', '2021-03-04', 45, NULL, 22, 56),
(91, 'T0032', '2021-03-14', 59, NULL, 20, 56),
(92, 'T0033', '2021-03-03', 56, '2021-04-30', 11, 57),
(93, 'T0034', '2021-03-12', 32, NULL, 12, 51),
(94, 'T0035', '2021-03-10', 46, NULL, 21, 56),
(95, 'T0036', '2021-03-06', 29, NULL, 18, 55),
(96, 'T0037', '2021-03-13', 39, NULL, 15, 57),
(97, 'T0038', '2021-03-05', 22, NULL, 16, 56),
(98, 'T0039', '2021-03-05', 55, NULL, 17, 55),
(99, 'T0040', '2021-03-04', 49, NULL, 14, 57),
(100, 'T0041', '2021-03-09', 41, '2021-05-04', 19, 56),
(101, 'T0042', '2021-03-03', 47, NULL, 15, 57),
(102, 'T0043', '2021-03-01', 46, '2021-04-24', 14, 58),
(103, 'T0044', '2021-03-02', 22, '2021-04-07', 19, 56),
(104, 'T0045', '2021-03-14', 36, NULL, 10, 52),
(105, 'T0046', '2021-03-05', 23, NULL, 14, 56),
(106, 'T0047', '2021-03-04', 44, '2021-03-29', 18, 54),
(107, 'T0048', '2021-03-11', 34, NULL, 19, 57),
(108, 'T0049', '2021-03-09', 37, NULL, 14, 49),
(109, 'T0050', '2021-03-03', 49, NULL, 11, 55),
(110, 'T0051', '2021-03-04', 39, '2021-04-08', 21, 52),
(111, 'T0052', '2021-03-06', 49, NULL, 9, 54),
(112, 'T0053', '2021-03-13', 34, '2021-05-04', 17, 51),
(113, 'T0054', '2021-03-10', 38, NULL, 9, 55),
(114, 'T0055', '2021-03-02', 34, NULL, 21, 54),
(115, 'T0056', '2021-03-02', 27, '2021-04-22', 9, 58),
(116, 'T0057', '2021-03-02', 21, NULL, 13, 52),
(117, 'T0058', '2021-03-09', 27, '2021-05-06', 12, 55),
(118, 'T0059', '2021-03-04', 20, NULL, 15, 58),
(119, 'T0060', '2021-03-10', 54, NULL, 14, 52),
(120, 'T0061', '2021-03-01', 21, '2021-04-10', 11, 55),
(121, 'T0062', '2021-03-11', 28, '2021-04-27', 10, 56),
(122, 'T0063', '2021-03-06', 43, NULL, 11, 58),
(123, 'T0064', '2021-03-12', 52, NULL, 13, 52),
(124, 'T0065', '2021-03-04', 25, NULL, 12, 54),
(125, 'T0066', '2021-03-09', 49, '2021-05-04', 22, 56),
(126, 'T0067', '2021-03-06', 44, NULL, 19, 50),
(127, 'T0068', '2021-03-09', 53, '2021-04-08', 14, 54),
(128, 'T0069', '2021-03-05', 36, NULL, 11, 55),
(129, 'T0070', '2021-03-03', 52, '2021-04-01', 19, 56),
(130, 'T0071', '2021-03-06', 27, NULL, 19, 56),
(131, 'T0072', '2021-03-05', 33, '2021-04-17', 13, 53),
(132, 'T0073', '2021-03-08', 44, NULL, 21, 58),
(133, 'T0074', '2021-03-03', 59, NULL, 16, 58),
(134, 'T0075', '2021-03-13', 58, '2021-04-26', 16, 58),
(135, 'T0076', '2021-03-05', 24, '2021-03-31', 21, 51),
(136, 'T0077', '2021-03-12', 25, '2021-04-24', 17, 53),
(137, 'T0078', '2021-03-13', 52, NULL, 14, 57),
(138, 'T0079', '2021-03-09', 56, NULL, 17, 52),
(139, 'T0080', '2021-03-01', 57, NULL, 13, 53),
(140, 'T0081', '2021-03-12', 51, NULL, 13, 56),
(141, 'T0082', '2021-03-11', 57, NULL, 13, 55),
(142, 'T0083', '2021-03-11', 30, NULL, 17, 51),
(143, 'T0084', '2021-03-12', 20, NULL, 20, 58),
(144, 'T0085', '2021-03-12', 57, '2021-05-07', 11, 51),
(145, 'T0086', '2021-03-11', 45, '2021-05-04', 18, 56),
(146, 'T0087', '2021-03-06', 38, '2021-04-11', 14, 53),
(147, 'T0088', '2021-03-14', 38, '2021-04-24', 18, 58),
(148, 'T0089', '2021-03-13', 28, NULL, 12, 56),
(149, 'T0090', '2021-03-11', 36, '2021-04-12', 22, 52),
(150, 'T0091', '2021-03-07', 31, NULL, 12, 57),
(151, 'T0092', '2021-03-01', 28, NULL, 21, 52),
(152, 'T0093', '2021-03-04', 58, '2021-04-06', 12, 54),
(153, 'T0094', '2021-03-11', 42, '2021-04-18', 20, 57),
(154, 'T0095', '2021-03-12', 47, NULL, 11, 50),
(155, 'T0096', '2021-03-03', 40, '2021-04-21', 13, 56),
(156, 'T0097', '2021-03-03', 29, NULL, 19, 51),
(157, 'T0098', '2021-03-01', 33, '2021-04-15', 10, 57),
(158, 'T0099', '2021-03-08', 47, NULL, 20, 51),
(159, 'T0100', '2021-03-07', 43, NULL, 20, 55);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
                            `id` bigint(20) NOT NULL,
                            `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Chính trị – Pháp luật'),
(2, 'Khoa học công nghệ – Kinh tế'),
(3, 'Văn học nghệ thuật'),
(4, 'Văn hóa xã hội – Lịch sử'),
(5, 'Giáo trình'),
(6, 'Truyện, tiểu thuyết'),
(7, 'Tâm lý, tâm linh, tôn giáo'),
(8, 'Sách thiếu nhi');

-- --------------------------------------------------------

--
-- Table structure for table `category_book`
--

CREATE TABLE `category_book` (
                                 `Category_id` bigint(20) NOT NULL,
                                 `books_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
    `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(260);

-- --------------------------------------------------------

--
-- Table structure for table `librarian`
--

CREATE TABLE `librarian` (
                             `id` bigint(20) NOT NULL,
                             `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                             `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                             `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                             `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                             `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                             `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `librarian`
--

INSERT INTO `librarian` (`id`, `code`, `email`, `name`, `password`, `phone`, `username`) VALUES
(49, 'L0011', 'admin@admin.com', 'Administrator', 'admin', '0334696355', 'admin'),
(50, 'L0001', 'chungdh883132@gmail.com', 'Đặng Hải Chung', 'ZZchun1@@', '0334696355', 'chungdh883132'),
(51, 'L0002', 'tuannm383409@gmail.com', 'Ngô Minh Tuấn', 'ZZtuan1@@', '0396785753', 'tuannm383409'),
(52, 'L0003', 'mylb893027@gmail.com', 'Lê Bảo My', 'ZZmylb1@@', '0770070143', 'mylb893027'),
(53, 'L0004', 'nhathd373219@gmail.com', 'Hồ Đức Nhật', 'ZZnhat1@@', '0776247336', 'nhathd373219'),
(54, 'L0005', 'kypt626357@gmail.com', 'Phi Thanh Kỳ', 'ZZkypt1@@', '0339101488', 'kypt626357'),
(55, 'L0006', 'minhdg381394@gmail.com', 'Đinh Gia Minh', 'ZZminh1@@', '0782351372', 'minhdg381394'),
(56, 'L0007', 'hoaipd708111@gmail.com', 'Phan Đình Hoài', 'ZZhoai1@@', '0376232672', 'hoaipd708111'),
(57, 'L0008', 'phuongkq392595@gmail.com', 'Khương Quang Phượng', 'ZZphuo1@@', '0762665311', 'phuongkq392595'),
(58, 'L0009', 'mongct489027@gmail.com', 'Cao Thạch Mộng', 'ZZmong1@@', '0354103610', 'mongct489027'),
(59, 'L0010', 'buukt974467@gmail.com', 'Khương Thái Bữu', 'ZZbuuk1@@', '0765828496', 'buukt974467');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
                           `id` bigint(20) NOT NULL,
                           `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                           `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                           `major` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                           `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                           `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `code`, `email`, `major`, `name`, `phone`) VALUES
(160, 'S0001', 'tamnm326177@gmail.com', 'Thiết kế đồ họa', 'Nguyễn Mạnh Tâm', '0569554316'),
(161, 'S0002', 'chieudh343896@gmail.com', 'Kỹ sư phần mềm', 'Đặng Hoàng Chiểu', '0325346011'),
(162, 'S0003', 'nhilt993506@gmail.com', 'An toàn thông tin', 'Lưu Thiên Nhi', '0328144677'),
(163, 'S0004', 'hunghg555024@gmail.com', 'An toàn thông tin', 'Hoàng Gia Hùng', '0326126875'),
(164, 'S0005', 'thienht408060@gmail.com', 'An toàn thông tin', 'Hứa Thiên Thiên', '0815901808'),
(165, 'S0006', 'gioikq266630@gmail.com', 'An toàn thông tin', 'Khương Quốc Giới', '0704153812'),
(166, 'S0007', 'huanld162309@gmail.com', 'An toàn thông tin', 'Lý Đông Huân', '0810196714'),
(167, 'S0008', 'nhientt528795@gmail.com', 'Kỹ sư phần mềm', 'Triệu Thông Nhiên', '0336346882'),
(168, 'S0009', 'dudm334567@gmail.com', 'Kỹ sư phần mềm', 'Đặng Minh Du', '0353712130'),
(169, 'S0010', 'taopm709903@gmail.com', 'Kỹ sư phần mềm', 'Phan Minh Tảo', '0883765741'),
(170, 'S0011', 'anch223507@gmail.com', 'Kỹ sư phần mềm', 'Cao Huy An', '0810596658'),
(171, 'S0012', 'phongps874332@gmail.com', 'Kỹ sư phần mềm', 'Phi Sơn Phong', '0332136904'),
(172, 'S0013', 'phuonghn996828@gmail.com', 'Kỹ sư phần mềm', 'Hồ Nhân Phương', '0586174456'),
(173, 'S0014', 'sieuht771025@gmail.com', 'Nhà hàng khách sạn', 'Hoàng Thanh Siêu', '0394098836'),
(174, 'S0015', 'hoaidg559561@gmail.com', 'An toàn thông tin', 'Đinh Gia Hoài', '0381774949'),
(175, 'S0016', 'vienhc959833@gmail.com', 'An toàn thông tin', 'Hứa Chí Viên', '0938413321'),
(176, 'S0017', 'chaukd984827@gmail.com', 'An toàn thông tin', 'Khương Đình Châu', '0350020050'),
(177, 'S0018', 'giangvn639279@gmail.com', 'Nhà hàng khách sạn', 'Vương Ngọc Giang', '0374864192'),
(178, 'S0019', 'truckt386003@gmail.com', 'Nhà hàng khách sạn', 'Khương Thụ Trực', '0374658885'),
(179, 'S0020', 'linhvc830515@gmail.com', 'An toàn thông tin', 'Vương Chí Linh', '0821271446'),
(180, 'S0021', 'giangpk260610@gmail.com', 'An toàn thông tin', 'Phi Khôi Giang', '0960939357'),
(181, 'S0022', 'duonglh276051@gmail.com', 'An toàn thông tin', 'Lưu Hạo Đường', '0584914750'),
(182, 'S0023', 'vinhdt236244@gmail.com', 'Kỹ sư phần mềm', 'Đỗ Thụ Vinh', '0398917530'),
(183, 'S0024', 'khuyenlk245458@gmail.com', 'Kỹ sư phần mềm', 'Lê Khôi Khuyến', '0927978489'),
(184, 'S0025', 'diepta118640@gmail.com', 'Kỹ sư phần mềm', 'Tạ Anh Điệp', '0784251521'),
(185, 'S0026', 'sangtc519702@gmail.com', 'Nhà hàng khách sạn', 'Triệu Chiến Sang', '0793187877'),
(186, 'S0027', 'quyentt621561@gmail.com', 'Nhà hàng khách sạn', 'Từ Thành Quyên', '0349652587'),
(187, 'S0028', 'tandg403602@gmail.com', 'Thiết kế đồ họa', 'Đặng Gia Tấn', '0360566069'),
(188, 'S0029', 'quynhhd504963@gmail.com', 'Nhà hàng khách sạn', 'Hoàng Đông Quỳnh', '0978966450'),
(189, 'S0030', 'thaiht317882@gmail.com', 'Nhà hàng khách sạn', 'Hồ Thạch Thái', '0843669579'),
(190, 'S0031', 'quyendt340194@gmail.com', 'Kỹ sư phần mềm', 'Đặng Trung Quyên', '0358377935'),
(191, 'S0032', 'myus793853@gmail.com', 'Kỹ sư phần mềm', 'Ung Sơn My', '0929857406'),
(192, 'S0033', 'dongth979423@gmail.com', 'Kỹ sư phần mềm', 'Từ Huy Đông', '0908118270'),
(193, 'S0034', 'khanhhk829728@gmail.com', 'Kỹ sư phần mềm', 'Hồ Kiến Khanh', '0789741972'),
(194, 'S0035', 'khavt681078@gmail.com', 'An toàn thông tin', 'Vương Thạch Kha', '0858146479'),
(195, 'S0036', 'triht153878@gmail.com', 'An toàn thông tin', 'Huỳnh Thiện Tri', '0569537919'),
(196, 'S0037', 'myhk661685@gmail.com', 'An toàn thông tin', 'Huỳnh Khôi My', '0852242077'),
(197, 'S0038', 'donghh761480@gmail.com', 'Nhà hàng khách sạn', 'Huỳnh Hùng Đông', '0566406598'),
(198, 'S0039', 'nhienhm409534@gmail.com', 'Kỹ sư phần mềm', 'Hoàng Mạnh Nhiên', '0980229793'),
(199, 'S0040', 'nguyetbq501508@gmail.com', 'Kỹ sư phần mềm', 'Bùi Quang Nguyệt', '0779641322'),
(200, 'S0041', 'ngunh851962@gmail.com', 'Kỹ sư phần mềm', 'Ngô Huy Ngữ', '0945948725'),
(201, 'S0042', 'anhdh113452@gmail.com', 'Nhà hàng khách sạn', 'Dương Hạo Anh', '0702496116'),
(202, 'S0043', 'bangha658899@gmail.com', 'Thiết kế đồ họa', 'Huỳnh Anh Bằng', '0799156615'),
(203, 'S0044', 'taihg434175@gmail.com', 'Nhà hàng khách sạn', 'Huỳnh Gia Tài', '0977570614'),
(204, 'S0045', 'nguyettm431214@gmail.com', 'An toàn thông tin', 'Tăng Mạnh Nguyệt', '0349181965'),
(205, 'S0046', 'quangpt831460@gmail.com', 'An toàn thông tin', 'Phi Thạch Quang', '0385636190'),
(206, 'S0047', 'hangcb144479@gmail.com', 'An toàn thông tin', 'Cao Bảo Hằng', '0386473471'),
(207, 'S0048', 'chunghn564470@gmail.com', 'Thiết kế đồ họa', 'Hứa Ngọc Chung', '0360698152'),
(208, 'S0049', 'khiemda171599@gmail.com', 'Nhà hàng khách sạn', 'Đặng Anh Khiêm', '0321565884'),
(209, 'S0050', 'buuhh963309@gmail.com', 'An toàn thông tin', 'Hoàng Hữu Bữu', '0836725433'),
(210, 'S0051', 'tienns234849@gmail.com', 'An toàn thông tin', 'Nguyễn Sơn Tiến', '0916026596'),
(211, 'S0052', 'uyth293414@gmail.com', 'An toàn thông tin', 'Tôn Hùng Uy', '0333959887'),
(212, 'S0053', 'vutb100406@gmail.com', 'An toàn thông tin', 'Tạ Bảo Vũ', '0338528841'),
(213, 'S0054', 'chungnc426263@gmail.com', 'Thiết kế đồ họa', 'Nguyễn Chiến Chung', '0931280165'),
(214, 'S0055', 'vodd438788@gmail.com', 'Thiết kế đồ họa', 'Đinh Đăng Võ', '0353141384'),
(215, 'S0056', 'lamvh950232@gmail.com', 'Marketing', 'Võ Hoàng Lâm', '0848978350'),
(216, 'S0057', 'ducvd384890@gmail.com', 'Kỹ sư phần mềm', 'Vũ Đăng Đức', '0323381550'),
(217, 'S0058', 'volt238772@gmail.com', 'Kỹ sư phần mềm', 'Lê Thụ Võ', '0394699446'),
(218, 'S0059', 'chiennt780451@gmail.com', 'Kỹ sư phần mềm', 'Nguyễn Thạch Chiến', '0947126139'),
(219, 'S0060', 'suups487280@gmail.com', 'Kỹ sư phần mềm', 'Phạm Sơn Sửu', '0975050529'),
(220, 'S0061', 'thuanmd190712@gmail.com', 'Kỹ sư phần mềm', 'Mạc Đức Thuận', '0701438507'),
(221, 'S0062', 'chihn726091@gmail.com', 'An toàn thông tin', 'Hồ Nhân Chi', '0365156958'),
(222, 'S0063', 'khuyendt873063@gmail.com', 'An toàn thông tin', 'Dương Thiên Khuyến', '0864115558'),
(223, 'S0064', 'habt808270@gmail.com', 'An toàn thông tin', 'Bùi Trung Hà', '0818608095'),
(224, 'S0065', 'chungtc861857@gmail.com', 'Thiết kế đồ họa', 'Trần Chiến Chung', '0852315114'),
(225, 'S0066', 'lelc516491@gmail.com', 'Thiết kế đồ họa', 'Lại Chiến Lệ', '0771729873'),
(226, 'S0067', 'sytd839400@gmail.com', 'Thiết kế đồ họa', 'Tôn Đình Sỹ', '0335994356'),
(227, 'S0068', 'quyhm568298@gmail.com', 'Thiết kế đồ họa', 'Huỳnh Mạnh Quy', '0924948155'),
(228, 'S0069', 'lanht963541@gmail.com', 'Thiết kế đồ họa', 'Hứa Thái Lân', '0857574341'),
(229, 'S0070', 'vott703015@gmail.com', 'Thiết kế đồ họa', 'Tạ Thụ Võ', '0779244981'),
(230, 'S0071', 'liemth127115@gmail.com', 'Marketing', 'Tôn Hoàng Liêm', '0880656735'),
(231, 'S0072', 'linhmh847424@gmail.com', 'Marketing', 'Mạc Hạo Linh', '0863135089'),
(232, 'S0073', 'cuongut238781@gmail.com', 'Marketing', 'Ung Thành Cường', '0949676102'),
(233, 'S0074', 'liemvd399276@gmail.com', 'Kỹ sư phần mềm', 'Võ Đông Liêm', '0789297491'),
(234, 'S0075', 'homt571703@gmail.com', 'Kỹ sư phần mềm', 'Mã Trung Hổ', '0375091151'),
(235, 'S0076', 'dunghp720799@gmail.com', 'Kỹ sư phần mềm', 'Hồ Phúc Dũng', '0795924466'),
(236, 'S0077', 'diepdt142222@gmail.com', 'Kỹ sư phần mềm', 'Đinh Thiện Điệp', '0332632367'),
(237, 'S0078', 'duongtt425331@gmail.com', 'Kỹ sư phần mềm', 'Tôn Thiện Đường', '0812202034'),
(238, 'S0079', 'manhld305102@gmail.com', 'Kỹ sư phần mềm', 'Lưu Đông Mạnh', '0776555548'),
(239, 'S0080', 'dinhth118439@gmail.com', 'Marketing', 'Tăng Huy Đình', '0854446330'),
(240, 'S0081', 'caocd830579@gmail.com', 'Marketing', 'Cao Đình Cao', '0905613941'),
(241, 'S0082', 'doanch720191@gmail.com', 'Marketing', 'Cao Hoàng Đoàn', '0979169405'),
(242, 'S0083', 'nghipd511479@gmail.com', 'Marketing', 'Phạm Đức Nghi', '0341127950'),
(243, 'S0084', 'viendk540125@gmail.com', 'Thiết kế đồ họa', 'Đặng Khôi Viên', '0361017229'),
(244, 'S0085', 'caoht682213@gmail.com', 'Marketing', 'Hứa Thiện Cao', '0581680828'),
(245, 'S0086', 'ngupt873030@gmail.com', 'Marketing', 'Phan Thụ Ngữ', '0362266528'),
(246, 'S0087', 'hieukh226437@gmail.com', 'Marketing', 'Khương Hải Hiếu', '0374046340'),
(247, 'S0088', 'quydd705696@gmail.com', 'Marketing', 'Đinh Đức Quy', '0799172434'),
(248, 'S0089', 'tungmh925456@gmail.com', 'Marketing', 'Mạc Hoàng Tùng', '0934532874'),
(249, 'S0090', 'anpt502556@gmail.com', 'Kỹ sư phần mềm', 'Phan Thông Ân', '0888956249'),
(250, 'S0091', 'mipt904484@gmail.com', 'Kỹ sư phần mềm', 'Phạm Thái Mi', '0792244553'),
(251, 'S0092', 'dinhnm240675@gmail.com', 'Kỹ sư phần mềm', 'Ngô Mạnh Đình', '0383839364'),
(252, 'S0093', 'cobc509030@gmail.com', 'Marketing', 'Bùi Chí Cơ', '0376089666'),
(253, 'S0094', 'vanvg698024@gmail.com', 'Marketing', 'Vương Gia Văn', '0980393984'),
(254, 'S0095', 'hienhd948725@gmail.com', 'Marketing', 'Hoàng Đăng Hiền', '0859937672'),
(255, 'S0096', 'doancd977311@gmail.com', 'Marketing', 'Cao Đức Đoàn', '0817589656'),
(256, 'S0097', 'nghent498970@gmail.com', 'Marketing', 'Ngô Thiên Nghệ', '0929170987'),
(257, 'S0098', 'lanhg474356@gmail.com', 'Thiết kế đồ họa', 'Hứa Gia Lân', '0358356114'),
(258, 'S0099', 'duchq802627@gmail.com', 'Kỹ sư phần mềm', 'Hứa Quang Đức', '0961504650'),
(259, 'S0100', 'liemmh880747@gmail.com', 'Kỹ sư phần mềm', 'Mã Huy Liêm', '0777188779');

-- --------------------------------------------------------

--
-- Table structure for table `student_bookloan`
--

CREATE TABLE `student_bookloan` (
                                    `Student_id` bigint(20) NOT NULL,
                                    `bookLoans_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `student_bookloan`
--

INSERT INTO `student_bookloan` (`Student_id`, `bookLoans_id`) VALUES
(160, 60),
(160, 61),
(160, 62),
(160, 63),
(160, 64),
(160, 65),
(160, 66),
(160, 67),
(160, 68),
(160, 69),
(161, 70),
(161, 71),
(161, 72),
(161, 73),
(161, 74),
(161, 75),
(161, 76),
(161, 77),
(161, 78),
(161, 79),
(161, 80),
(161, 81),
(161, 82),
(161, 83),
(161, 84),
(161, 85),
(161, 86),
(161, 87),
(162, 88),
(162, 89),
(162, 90),
(162, 91),
(163, 92),
(163, 93),
(163, 94),
(163, 95),
(163, 96),
(163, 97),
(163, 98),
(163, 99),
(163, 100),
(163, 101),
(163, 102),
(163, 103),
(163, 104),
(163, 105),
(163, 106),
(163, 107),
(163, 108),
(163, 109),
(164, 110),
(164, 111),
(164, 112),
(164, 113),
(164, 114),
(164, 115),
(164, 116),
(164, 117),
(164, 118),
(164, 119),
(164, 120),
(165, 121),
(165, 122),
(165, 123),
(165, 124),
(165, 125),
(165, 126),
(165, 127),
(165, 128),
(165, 129),
(165, 130),
(165, 131),
(165, 132),
(165, 133),
(165, 134),
(165, 135),
(165, 136),
(165, 137),
(165, 138),
(166, 139),
(166, 140),
(166, 141),
(166, 142),
(166, 143),
(166, 144),
(166, 145),
(166, 146),
(166, 147),
(166, 148),
(166, 149),
(166, 150),
(166, 151),
(166, 152),
(166, 153),
(166, 154),
(166, 155),
(166, 156),
(166, 157),
(166, 158),
(166, 159);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
    ADD PRIMARY KEY (`id`),
  ADD KEY `FKe4psgwi6953wvbqxruna75yax` (`category_id`);

--
-- Indexes for table `booklist`
--
ALTER TABLE `booklist`
    ADD PRIMARY KEY (`id`),
  ADD KEY `FKkmwfnuh11l1qwuljca03f12pp` (`book_id`);

--
-- Indexes for table `bookloan`
--
ALTER TABLE `bookloan`
    ADD PRIMARY KEY (`id`),
  ADD KEY `FK3bdiyxhogodqt8hyubldlg17l` (`book_id`),
  ADD KEY `FKk6u0ujlc3bf42h2gdscx9gxfw` (`librarian_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_book`
--
ALTER TABLE `category_book`
    ADD UNIQUE KEY `UK_7s26lwej1frj8abilkhr4ck0x` (`books_id`),
    ADD KEY `FKstrlu6w9xjx6pb3ipe904un8d` (`Category_id`);

--
-- Indexes for table `librarian`
--
ALTER TABLE `librarian`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_bookloan`
--
ALTER TABLE `student_bookloan`
    ADD UNIQUE KEY `UK_sy78e2207cmmtaenbf6ie9dy9` (`bookLoans_id`),
    ADD KEY `FKgapgaswb66bfei3rtu1wn9ja3` (`Student_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
